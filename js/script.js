let tabs = document.querySelectorAll('.tabs-title');
let text = document.querySelectorAll('.tabs-content-text');

//Додаємо клас при натисканні на вкладку(таб) та показуємо відповідний текст
tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        //Перебираємо таби та видаляємо клас 'active' у всіх елементів
        tabs.forEach(t => {
          t.classList.remove('active');
        });
        //Додаємо клас 'active' до табу який був натиснутий
        tab.classList.add('active');
        //Перебераємо елементи з текстом
        text.forEach(elem => {
            //Видаляємо клас 'active' у всіх елементів
            elem.classList.remove('active')
            //Перевіряємо співвідношення data-атрибутів
            if(tab.dataset.name === elem.dataset.name) {
                //до відповідного елеммента з текстом додаємо клас 'active'
                elem.classList.add('active');
            }
        });
    });
});

const btn = document.querySelector('.btn');

// Функція для збереження стану в LocalStorage
const saveStateToLocalStorage = () => {
    const state = {
        btnActive: btn.classList.contains('active'), // Запам'ятовуємо, чи має кнопка клас 'active'
    };
    localStorage.setItem('appState', JSON.stringify(state)); // Зберігаємо стан у LocalStorage
};

// Функція для відновлення стану з LocalStorage
const restoreStateFromLocalStorage = () => {
    const savedState = localStorage.getItem('appState'); // Отримуємо збережений стан з LocalStorage

    if (savedState) {
        const state = JSON.parse(savedState); // Розпаковуємо збережений стан

        if (state.btnActive) {
            // Якщо збережений стан має активну кнопку, то відповідні елементи отримують необхідні класи та стилі
            btn.classList.add('active');
            tabs.forEach(item => {
                item.classList.add('black');
            });
            text.forEach(elem => {
                elem.classList.add('black');
            });
            document.body.style.background = '#6f5327';
        }
    }
};
// Додавання обробника події на клік кнопки
btn.addEventListener('click', () => {
    if (btn.classList.contains('active')) {
        // Якщо кнопка має активний клас, то змінюємо стан елементів та очищуємо LocalStorage
        btn.classList.remove('active');
        tabs.forEach(item => {
            item.classList.remove('black');
        });
        text.forEach(elem => {
            elem.classList.remove('black');
        });
        document.body.style.background = '#fff';
        localStorage.removeItem('appState'); // Очищаємо LocalStorage, щоб відмінити збережений стан
    } else {
        // Якщо кнопка не має активний клас, то змінюємо стан елементів та зберігаємо його в LocalStorage
        btn.classList.add('active');
        tabs.forEach(item => {
            item.classList.add('black');
        });
        text.forEach(elem => {
            elem.classList.add('black');
        });
        document.body.style.background = '#6f5327';
    }

    saveStateToLocalStorage(); // Зберігаємо актуальний стан у LocalStorage
});

// Відновлюємо збережений стан при завантаженні сторінки
restoreStateFromLocalStorage();